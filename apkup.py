#!/usr/bin/python3.5

import os, json
from colorama import Fore, Back, Style
from bs4 import BeautifulSoup

from urllib.request import Request, urlopen, urlretrieve
from urllib.error import HTTPError
from urllib.error import URLError

import smtplib

from email.message import EmailMessage
from email.headerregistry import Address
from email.utils import make_msgid

class Phone:
    def __init__(self, phoneDescription, phoneUID, userUID, email, applist):
        self.phoneDescription = phoneDescription
        self.phoneUID = phoneUID
        self.userUID = userUID
        self.email = email
        self.applist = applist
        
    def list_apps(self):
        return self.applist
    
    def send_notifications(self):
        pass
    
    def show_info(self):
        return "{} {} {} {}".format(self.phoneDescription, self.phoneUID, self.userUID, self.email)

class APKapp:
    def __init__(self, appUID, name, installed_version, current_version, developer, sourceURL, filename, mirror, variant, soup_mod):
        self.appUID = appUID
        self.name = name
        self.installed_version = installed_version
        self.current_version = current_version
        self.developer = developer
        self.sourceURL = sourceURL
        self.filename = filename
        self.mirror = mirror
        self.variant = variant
        self.soup_mod = soup_mod
        
    def show_info(self):
        return "{} {} {} {} {}".format(self.name, self.installed_version, self.current_version, self.developer, self.variant)

    def check_for_update(self):
        
        return self
    
    def compare_version_strings(self):
        # compare version strings as integers
        installedVersion = self.installed_version.split(".")
        #print("Installed Version : ", installedVersion)
        currentVersion = self.current_version.split(".")
        # print("Current Version : ", currentVersion)
        
        
        updateExists = False
        if len(installedVersion) == len(currentVersion):
            #print("test OK")
            i = 0
            while i < len(installedVersion):
                #print(i)
                if installedVersion[i] < currentVersion[i]:
                    updateExists = True
                    #print(installedVersion[i], " ", currentVersion[i], "newer")
                else:
                    #print(installedVersion[i], " ", currentVersion[i], "same")
                    pass
                i += 1
        else:
            test = 0
            for i in installedVersion:
                test += int(i)
            if test == 0:
                print("WARNING: no version exists")
                updateExists = True
            else:
                print("Error: version strings don't match")
        
        return updateExists
    
    #def download_update(self):
        #print(self.name, "Downloading Update")
        
        ##return self

#------------- define file constants ---------------#    

# define the absolute path as baseDir
baseDir = os.path.realpath(__file__).split("apkup")[0]+"apkup"

print("Base Directory : ", baseDir)

#------------- define some User Interface constants ---------------#    
#TODO: read these from first record in phonelist.json
userUID = 100
global phoneUID
phoneUID = 1

# ------- functions to load / reload phone and app lists -----------#    


def LoadPhones():
    # TODO: make this absolute path variable
    with open("phonelist.json") as json_file:
        text = json_file.read()
        # print(text)
        phonelist = json.loads(text)

    #print(phonelist)
    return phonelist


def LoadApps():
    # TODO: make this absolute path variable
    with open("apklist.json") as json_file:
        text = json_file.read()
        # print(text)
        applist = json.loads(text)
    return applist


# ---------------------- The User Interface ------------------------#

def clrscr():
    # TODO: make this portable
    #os.system('cls')  # For Windows
    os.system('clear')  # For Linux/OS X


def MainMenu():
    global phoneUID
    optionList = ["Select Phone", "Edit Phone"]
    menu_done = False
    selection = 0
    while not menu_done:
        selection = DrawMenu(optionList)
        if selection == 0:
            menu_done = True
        elif selection == 1:
            SelectPhone()
        elif selection == 2:
            EditPhone()
        else:
            selection = DrawMenu(optionList)
            print(Back.BLACK + Fore.RED + "error, try again" + Style.RESET_ALL)


def DrawMenu(optionList):
    clrscr()
    print(Back.BLACK + Fore.BLUE + "Phone Selected : " + str(phoneUID) + Style.RESET_ALL)
    selection = 0
    for i,option in enumerate(optionList):
        if i+1 == selection:
            print(Back.GREEN + Fore.RED + str(i + 1), " : ", option + Style.RESET_ALL)
        else:
            print(Back.BLACK + Fore.WHITE + str(i + 1), " : ", option + Style.RESET_ALL)

    selection = input(Back.BLACK + Fore.GREEN + "Select option (or q): ")
    try:
        selection = int(selection)
    except:
        selection = 0
        menu_done = True
    
    return selection


def SelectPhone():
    global phoneUID
    #FIXME: duplicate loading of files
    phonelist = LoadPhones()
    menu_done = False
    while not menu_done:
        clrscr()
        # list the phones, highlighting selected
        phoneCounter = 0
        
        for phone in phonelist["phones"]:
            phoneCounter += 1
            menu_text = str(phoneCounter) + "   Phone :  " + str(phone["phoneUID"]) + " Description : " + phone["phoneDescription"]
            if phone["phoneUID"] == phoneUID:
                # highlight this line
                print(Back.GREEN + Fore.RED + menu_text + Style.RESET_ALL)
                
            else:
                # standard colours
                print(Style.RESET_ALL + menu_text)
        
        selection = input(Back.BLACK + Fore.GREEN + "Select Phone to modify (or q): ")
        try:
            selection = int(selection)
        except:
            selection = 0

        if selection == 0:
            menu_done = True
            print(phoneUID, " selected")
        elif selection > phoneCounter:
            print(Back.BLACK + Fore.RED + "error, try again" + Style.RESET_ALL)
            #TODO: wait to display or flash this warning ???
        else:
            phoneUID = selection


def EditPhone():
    phonelist = LoadPhones()
    # read the attributes of the selected phone

    for phone in phonelist["phones"]:
        if phoneUID == phone["phoneUID"]:
            # set these to the active variables
            userUID = phone["userUID"]
            #phoneUID = phone["phoneUID"]
            phoneDescription = phone["phoneDescription"]
            myemail = phone["email"]
            myapps = phone["apps"]
            myphone = Phone(phoneDescription, userUID, phoneUID, myemail, myapps)
            EditMenuItems = ["Phone Description : " + phoneDescription, "Notification email : " + myemail, "Add or Remove Apps : " + str(myapps)]

    menu_done = False
    while not menu_done:
        selection = DrawMenu(EditMenuItems)
        print(Style.RESET_ALL)
        try:
            selection = int(selection)
        except:
            selection = 0

        for i, phone in enumerate(phonelist["phones"]):
                if phone["phoneUID"] == phoneUID:
                    phoneIndex = i

        if selection == 0:
            menu_done = True
        elif selection > len(EditMenuItems):
            print(Back.BLACK + Fore.RED + "error, try again" + Style.RESET_ALL)
            #TODO: wait to display this warning
        elif selection == 1:
            # Description
            phonelist = EditDescription(phoneIndex)
            #input()
        elif selection == 2:
            # email
            phonelist = EditEmail(phoneIndex)
            #input()
        elif selection == 3:
            print(phone["apps"])
            AppMenuItems = ["Add app", "Remove app"]
            selection = DrawMenu(AppMenuItems)
            #print(selection)
            
            phonelist = AddApp(phoneIndex)
        else:
            print("something went wrong")
            #input()


def EditDescription(phoneIndex):
    phonelist = LoadPhones()
    
    oldText = phonelist["phones"][phoneIndex]["phoneDescription"]
    newText = input("Enter new Description [" + oldText + "]: ")
    if newText:
        phonelist["phones"][phoneIndex]["phoneDescription"] = newText
        with open("phonelist.json", 'w') as outfile:
            json.dump(phonelist, outfile)
    else:
        print("Description unchanged")
        
    return phonelist

def EditEmail(phoneIndex):
    phonelist = LoadPhones()
    
    oldText = phonelist["phones"][phoneIndex]["email"]
    newText = input("Enter new email [" + oldText + "]: ")
    if newText:
        phonelist["phones"][phoneIndex]["phoneDescription"] = newText
        with open("phonelist.json", 'w') as outfile:
            json.dump(phonelist, outfile)
    else:
        print("Email unchanged")
        
    return phonelist
    
def AddApp(phoneIndex):
    # list available apps
    addAppItems = []
    applist = LoadApps()
    for app in applist["apps"]:
        addAppItems.append(app["name"])
    
    selection = DrawMenu(addAppItems)
    
    phonelist = LoadPhones()
    phonelist["phones"][phoneIndex]["apps"].append(addAppItems[selection-1])
    
    
    #TODO: prevent duplicates
    
    with open("phonelist.json", 'w') as outfile:
        json.dump(phonelist, outfile)
    
    return phonelist

def RemoveApp(phoneIndex):
    #TODO: this
    return phonelist

# ----------------- end of user interface --------------------#

# ----------------- begin web update section --------------------#

def CheckUpdateSource(myapp):
    result = RequestHTML(myapp.sourceURL)
    soup = BeautifulSoup(result.read(),"html5lib")
    
    
    
    if myapp.name == "VPN by Private Internet Access":
    
        soupResult = soup.find_all(attrs={"id": "download_title"})

        filename, checksum = soup.find_all("code")

        print(filename.text)
        print(checksum.text)
        
        
        
    elif myapp.name == "Signal":
        pass
    else:
        pass
    
    # write
        


def CheckUpdateAPKmirror(myapp):
        
        updateExists = False
        appName = myapp.name
        searchURL = myapp.mirror
        if testing:
            soup = write_html_temp(appName, searchURL)
        
        else: # get a fresh copy from the internet
            result = RequestHTML(searchURL)
            soup = BeautifulSoup(result.read(),"html5lib")

        
        searchResult = soup.find_all(attrs={"class": "appRowTitle"})
        #print(searchResult.text)
        
        
        #print(searchResult[myapp.soup_mod].find('a')['href'])
        
        print(searchResult[myapp.soup_mod].text.rsplit(' ', 1)[0])
        #myapp.title = searchResult[myapp.soup_mod].text.rsplit(' ', 1)[0]
        #for item in searchResults:
            #print(item.find('a')['href'])
            #input()
        
        # use the nth result according to soup_mod    
        # remove \n newline from string
        versionString = searchResult[myapp.soup_mod].text.rstrip()
        
        # extract the version number from the end of the title
        versionString = versionString.split(" ")[-1]

        print(versionString)
        
        myapp.current_version = versionString
       
        updateExists = myapp.compare_version_strings()
        return(updateExists, myapp)


def RequestHTML(searchURL):
    print("Fetching ", searchURL)

    try:
        hdr = {'User-Agent': 'Mozilla/5.0'}
        request = Request(searchURL, headers=hdr)
        result = urlopen(request)
    except HTTPError as e:
        print(e)
    except URLError:
        print("Server down or incorrect domain")
    else:
        pass
        # get the title
    
    return result

def write_html_temp(appName, searchURL):
    outPath = appName + ".html"
    print("Output filename : ", outPath)
    
    if os.path.exists(outPath):
        try:
            with open(outPath) as result:
                print("Using cached copy")
                soup = BeautifulSoup(result.read(),"html5lib")
                #print(str(soup))
        except:
            print("Could not load cached version")
    else:
        result = RequestHTML(searchURL)
        # save the html for testing
        soup = BeautifulSoup(result.read(),"html5lib")
        print("New file: ", outPath)
        try:
            with open(outPath, 'w') as out_file:
                out_file.write(str(soup))

        except FileNotFoundError:
            print("File Not Found: ", outPath)

    return soup



def SendNotifications(notifications):
    phonelist = LoadPhones()
    applist = LoadApps()
    
    
        
    print("Send notifications for the following apps")
    for app in notifications:
        print("  ", app)
        #TODO: check phones for apps to be notified
    
        
    for phone in phonelist["phones"]:
        
        myphone = Phone(phone["phoneDescription"], phone["phoneUID"], phone["userUID"], phone["email"], phone["apps"])
        
    
        
        
        if set(notifications) & set(myphone.applist):
            print("found a match")           
            notify = True

        else:
            notify = False
    
    
        if notify:
            print("Sending message to ", myphone.email)
            
            # construct the message
            
            
            message = """From: App Notifications <notifications@fosscairns.orgfree.com>
                To: <""" + myphone.email +  """>
                MIME-Version: 1.0
                Content-type: text/html
                
                Subject: App updates are available for your phone
                <h1>The following apps have been updated:</h1>"""
            
            for installed_app in myphone.applist:
                if installed_app in notifications:
                    message += "\n    " + installed_app
                    for app in applist["apps"]:
                        if app["name"] == installed_app:
                            message += "\n           <a href=" + app["mirror"] + "> " + "Download from APKMirror" + "</a>"
                
            message += "\n\nFollow the link to install the latest version"
            
            
            print(message)

            

            try:
                smtpObj = smtplib.SMTP('localhost')
                smtpObj.sendmail("notifications@fosscairns.orgfree.com", myphone.email, message)
                print("Successfully sent email")
            except SMTPException:
                print("Error: unable to send email")

    

def DownloadAPK(target):
    return("nothing downloaded")





# TODO: start in interactive mode only if an option is passed at startup
# -------- if we're in interactive mode, start the menu -----#

MainMenu()

print(Style.RESET_ALL)

# -------- end of interactive mode, start the updates -----#

# build a list of which apps need to be checked
# from the list of installed apps on each phone
phonelist = LoadPhones()
#print(phoneUID)
appList = []
notifications = []
for phone in phonelist["phones"]:
    # make a combined list of installed apps 
    for app in phone["apps"]:
        if not app in appList:
            appList.append(app)

print("Counted ", len(appList), " apps")
for app in appList:
    print("  ", app)


# ---------- which apps to check? ------------#
#print("Apps installed on selected phone: ", Phone.list_apps(myphone))


# ---------- Testing Flag ------------#
testing = True


# ---------- check each app for updates ------------#

# open apklist.json
# TODO: make this absolute path variable
with open("apklist.json") as json_file:
    text = json_file.read()
    #print(text)
    apklist = json.loads(text)
    #print(apklist)

apklistModified = False
print()

for appIndex, app in enumerate(apklist["apps"]):
    if app["name"] in appList:
        #print(app["name"])
        myapp = APKapp(app["appUID"], app["name"], app["installed_version"], app["current_version"], app["developer"], app["sourceURL"], app["filename"], app["mirror"], app["variant"], app["soup_mod"])
        
        
        print()
        
        versionsDiffer = myapp.compare_version_strings()
        
        if versionsDiffer:
            print(myapp.show_info())
            print(myapp.name, "Not checking for new version")
        else:
            updateExists = False
            appName = myapp.name
                
            print("Checking ", appName)
            updateExists, myapp = CheckUpdateAPKmirror(myapp)
                
            if updateExists:
                print(myapp.name, " can be updated from ", myapp.mirror)
                #print(myapp.show_info())
                #input()
            else:
                print("Up To Date")

            if updateExists:
                #print("write the new version info to apklist object")
        
                print(app["name"], " will be modified")
                # add this app to notifications
                notifications.append(app["name"])
                
                #print(app["installed_version"])
                #print(myapp.current_version)
                #input()
                # update the apklist object with new info
                
                apklist["apps"][appIndex]["current_version"] = myapp.current_version
                apklist["apps"][appIndex]["installed_version"] = myapp.current_version
                
                #TODO: update sourceURL and Filename
                
                apklistModified = True
                
                
        
        

if apklistModified:
    # write the apklist.json file
    print("------------writing file-------------")
    #print(apklist)
    
    
    with open("apklist.json", 'w') as outfile:
        json.dump(apklist, outfile)
        print()
        print("apklist.json updated")
    #print("FILE WRITE DISABLED")
print()

SendNotifications(notifications)
# and download the new APK files:


print()
print("Done")







